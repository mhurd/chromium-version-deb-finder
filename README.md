Provides a method for getting the most recent Chromium Debian '.deb' installer file URLs for a given Chromium major / minor version, Debian version and architecture combination

See `index.test.js` for examples

You can also run `make test` or `make sample` to run the tests or a sample call in a Docker container
