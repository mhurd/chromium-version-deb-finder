jest.setTimeout(30000)

const { getDebUrlsForVersionAndArch } = require('./index')

describe('Chromium getDebUrlsForVersionAndArch arm64 .deb file url tests, Debian 11', () => {

  const DEBIAN_VERSION = 11;

  test('Chromium 97.0 arm64 .deb urls', async () => {
    expect(await getDebUrlsForVersionAndArch(97, 0, DEBIAN_VERSION, 'arm64')).toEqual([
      'http://snapshot.debian.org/archive/debian-security/20220124T070450Z/pool/updates/main/c/chromium/chromium_97.0.4692.99-1~deb11u2_arm64.deb',
      'http://snapshot.debian.org/archive/debian-security/20220124T070450Z/pool/updates/main/c/chromium/chromium-common_97.0.4692.99-1~deb11u2_arm64.deb'
    ])
  })

  test('Chromium 98.0 arm64 .deb urls', async () => {
    expect(await getDebUrlsForVersionAndArch(98, 0, DEBIAN_VERSION, 'arm64')).toEqual([
      'http://snapshot.debian.org/archive/debian-security/20220219T044031Z/pool/updates/main/c/chromium/chromium_98.0.4758.102-1~deb11u1_arm64.deb',
      'http://snapshot.debian.org/archive/debian-security/20220219T044031Z/pool/updates/main/c/chromium/chromium-common_98.0.4758.102-1~deb11u1_arm64.deb'
    ])
  })

  test('Chromium 99.0 arm64 .deb urls', async () => {
    expect(await getDebUrlsForVersionAndArch(99, 0, DEBIAN_VERSION, 'arm64')).toEqual([
      'http://snapshot.debian.org/archive/debian-security/20220328T233642Z/pool/updates/main/c/chromium/chromium_99.0.4844.84-1~deb11u1_arm64.deb',
      'http://snapshot.debian.org/archive/debian-security/20220328T233642Z/pool/updates/main/c/chromium/chromium-common_99.0.4844.84-1~deb11u1_arm64.deb',
    ])
  })

  test('Chromium 100.0 arm64 .deb urls', async () => {
    expect(await getDebUrlsForVersionAndArch(100, 0, DEBIAN_VERSION, 'arm64')).toEqual([
      'http://snapshot.debian.org/archive/debian-security/20220417T040536Z/pool/updates/main/c/chromium/chromium_100.0.4896.127-1~deb11u1_arm64.deb',
      'http://snapshot.debian.org/archive/debian-security/20220417T040536Z/pool/updates/main/c/chromium/chromium-common_100.0.4896.127-1~deb11u1_arm64.deb',
    ])
  })

  test('Chromium 101.0 arm64 .deb urls', async () => {
    expect(await getDebUrlsForVersionAndArch(101, 0, DEBIAN_VERSION, 'arm64')).toEqual([
      'http://snapshot.debian.org/archive/debian-security/20220517T153702Z/pool/updates/main/c/chromium/chromium_101.0.4951.64-1~deb11u1_arm64.deb',
      'http://snapshot.debian.org/archive/debian-security/20220517T153702Z/pool/updates/main/c/chromium/chromium-common_101.0.4951.64-1~deb11u1_arm64.deb',
    ])
  })

  test('Chromium 102.0 arm64 .deb urls', async () => {
    expect(await getDebUrlsForVersionAndArch(102, 0, DEBIAN_VERSION, 'arm64')).toEqual([
      'http://snapshot.debian.org/archive/debian-security/20220612T175238Z/pool/updates/main/c/chromium/chromium_102.0.5005.115-1~deb11u1_arm64.deb',
      'http://snapshot.debian.org/archive/debian-security/20220612T175238Z/pool/updates/main/c/chromium/chromium-common_102.0.5005.115-1~deb11u1_arm64.deb'
    ])
  })

  test('Chromium 103.0 arm64 .deb urls', async () => {
    expect(await getDebUrlsForVersionAndArch(103, 0, DEBIAN_VERSION, 'arm64')).toEqual([
      'http://snapshot.debian.org/archive/debian-security/20220722T181415Z/pool/updates/main/c/chromium/chromium_103.0.5060.134-1~deb11u1_arm64.deb',
      'http://snapshot.debian.org/archive/debian-security/20220722T181415Z/pool/updates/main/c/chromium/chromium-common_103.0.5060.134-1~deb11u1_arm64.deb'
    ])
  })

  test('Chromium 104.0 arm64 .deb urls', async () => {
    expect(await getDebUrlsForVersionAndArch(104, 0, DEBIAN_VERSION, 'arm64')).toEqual([
      'http://snapshot.debian.org/archive/debian-security/20220818T015631Z/pool/updates/main/c/chromium/chromium_104.0.5112.101-1~deb11u1_arm64.deb',
      'http://snapshot.debian.org/archive/debian-security/20220818T015631Z/pool/updates/main/c/chromium/chromium-common_104.0.5112.101-1~deb11u1_arm64.deb'
    ])
  })

  test('Chromium 105.0 arm64 .deb urls', async () => {
    expect(await getDebUrlsForVersionAndArch(105, 0, DEBIAN_VERSION, 'arm64')).toEqual([
      'http://snapshot.debian.org/archive/debian-security/20220916T075507Z/pool/updates/main/c/chromium/chromium_105.0.5195.125-1~deb11u1_arm64.deb',
      'http://snapshot.debian.org/archive/debian-security/20220916T075507Z/pool/updates/main/c/chromium/chromium-common_105.0.5195.125-1~deb11u1_arm64.deb'
    ])
  })

  test('Chromium 106.0 arm64 .deb urls', async () => {
    expect(await getDebUrlsForVersionAndArch(106, 0, DEBIAN_VERSION, 'arm64')).toEqual([
      'http://snapshot.debian.org/archive/debian-security/20221013T190404Z/pool/updates/main/c/chromium/chromium_106.0.5249.119-1~deb11u1_arm64.deb',
      'http://snapshot.debian.org/archive/debian-security/20221013T190404Z/pool/updates/main/c/chromium/chromium-common_106.0.5249.119-1~deb11u1_arm64.deb'
    ])
  })

  test('Chromium 107.0 arm64 .deb urls', async () => {
    expect(await getDebUrlsForVersionAndArch(107, 0, DEBIAN_VERSION, 'arm64')).toEqual([
      'http://snapshot.debian.org/archive/debian-security/20221128T142501Z/pool/updates/main/c/chromium/chromium_107.0.5304.121-1~deb11u1_arm64.deb',
      'http://snapshot.debian.org/archive/debian-security/20221128T142501Z/pool/updates/main/c/chromium/chromium-common_107.0.5304.121-1~deb11u1_arm64.deb'
    ])
  })

  test('Chromium 108.0 arm64 .deb urls', async () => {
    expect(await getDebUrlsForVersionAndArch(108, 0, DEBIAN_VERSION, 'arm64')).toEqual([
      'http://snapshot.debian.org/archive/debian-security/20221216T190906Z/pool/updates/main/c/chromium/chromium_108.0.5359.124-1~deb11u1_arm64.deb',
      'http://snapshot.debian.org/archive/debian-security/20221216T190906Z/pool/updates/main/c/chromium/chromium-common_108.0.5359.124-1~deb11u1_arm64.deb'
    ])
  })

  test('Chromium 109.0 arm64 .deb urls', async () => {
    expect(await getDebUrlsForVersionAndArch(109, 0, DEBIAN_VERSION, 'arm64')).toEqual([
      'http://snapshot.debian.org/archive/debian-security/20230114T170508Z/pool/updates/main/c/chromium/chromium_109.0.5414.74-2~deb11u1_arm64.deb',
      'http://snapshot.debian.org/archive/debian-security/20230114T170508Z/pool/updates/main/c/chromium/chromium-common_109.0.5414.74-2~deb11u1_arm64.deb'
    ])
  })

  test('Chromium 110.0 arm64 .deb urls', async () => {
    expect(await getDebUrlsForVersionAndArch(110, 0, DEBIAN_VERSION, 'arm64')).toEqual([
      'http://snapshot.debian.org/archive/debian-security/20230224T173549Z/pool/updates/main/c/chromium/chromium_110.0.5481.177-1~deb11u1_arm64.deb',
      'http://snapshot.debian.org/archive/debian-security/20230224T173549Z/pool/updates/main/c/chromium/chromium-common_110.0.5481.177-1~deb11u1_arm64.deb'
    ])
  })

  test('Chromium 111.0 arm64 .deb urls', async () => {
    expect(await getDebUrlsForVersionAndArch(111, 0, DEBIAN_VERSION, 'arm64')).toEqual([
      'http://snapshot.debian.org/archive/debian-security/20230324T220733Z/pool/updates/main/c/chromium/chromium_111.0.5563.110-1~deb11u1_arm64.deb',
      'http://snapshot.debian.org/archive/debian-security/20230324T220733Z/pool/updates/main/c/chromium/chromium-common_111.0.5563.110-1~deb11u1_arm64.deb'
    ])
  })

  test('Chromium 112.0 arm64 .deb urls', async () => {
    expect(await getDebUrlsForVersionAndArch(112, 0, DEBIAN_VERSION, 'arm64')).toEqual([
      'http://snapshot.debian.org/archive/debian-security/20230423T032736Z/pool/updates/main/c/chromium/chromium_112.0.5615.138-1~deb11u1_arm64.deb',
      'http://snapshot.debian.org/archive/debian-security/20230423T032736Z/pool/updates/main/c/chromium/chromium-common_112.0.5615.138-1~deb11u1_arm64.deb',
    ])
  })

  test('Chromium 113.0 arm64 .deb urls', async () => {
    expect(await getDebUrlsForVersionAndArch(113, 0, DEBIAN_VERSION, 'arm64')).toEqual([
      'http://snapshot.debian.org/archive/debian-security/20230517T212424Z/pool/updates/main/c/chromium/chromium_113.0.5672.126-1~deb11u1_arm64.deb',
      'http://snapshot.debian.org/archive/debian-security/20230517T212424Z/pool/updates/main/c/chromium/chromium-common_113.0.5672.126-1~deb11u1_arm64.deb',
    ])
  })

  test('Chromium 114.0 arm64 .deb urls', async () => {
    expect(await getDebUrlsForVersionAndArch(114, 0, DEBIAN_VERSION, 'arm64')).toEqual([
      'http://snapshot.debian.org/archive/debian-security/20230630T130729Z/pool/updates/main/c/chromium/chromium_114.0.5735.198-1~deb11u1_arm64.deb',
      'http://snapshot.debian.org/archive/debian-security/20230630T130729Z/pool/updates/main/c/chromium/chromium-common_114.0.5735.198-1~deb11u1_arm64.deb',
    ])
  })

  test('Chromium 115.0 arm64 .deb urls', async () => {
    expect(await getDebUrlsForVersionAndArch(115, 0, DEBIAN_VERSION, 'arm64')).toEqual([
      'http://snapshot.debian.org/archive/debian-security/20230805T172527Z/pool/updates/main/c/chromium/chromium_115.0.5790.170-1~deb11u1_arm64.deb',
      'http://snapshot.debian.org/archive/debian-security/20230805T172527Z/pool/updates/main/c/chromium/chromium-common_115.0.5790.170-1~deb11u1_arm64.deb',
    ])
  })

  test('Chromium 116.0 arm64 .deb urls', async () => {
    expect(await getDebUrlsForVersionAndArch(116, 0, DEBIAN_VERSION, 'arm64')).toEqual([
      'http://snapshot.debian.org/archive/debian-security/20230907T174637Z/pool/updates/main/c/chromium/chromium_116.0.5845.180-1~deb11u1_arm64.deb',
      'http://snapshot.debian.org/archive/debian-security/20230907T174637Z/pool/updates/main/c/chromium/chromium-common_116.0.5845.180-1~deb11u1_arm64.deb',
    ])
  })

  test('Chromium 117.0 arm64 .deb urls', async () => {
    expect(await getDebUrlsForVersionAndArch(117, 0, DEBIAN_VERSION, 'arm64')).toEqual([
      'http://snapshot.debian.org/archive/debian-security/20231006T223712Z/pool/updates/main/c/chromium/chromium_117.0.5938.149-1~deb11u1_arm64.deb',
      'http://snapshot.debian.org/archive/debian-security/20231006T223712Z/pool/updates/main/c/chromium/chromium-common_117.0.5938.149-1~deb11u1_arm64.deb',
    ])
  })

  test('Chromium 118.0 arm64 .deb urls', async () => {
    expect(await getDebUrlsForVersionAndArch(118, 0, DEBIAN_VERSION, 'arm64')).toEqual([
      'http://snapshot.debian.org/archive/debian-security/20231027T235634Z/pool/updates/main/c/chromium/chromium_118.0.5993.117-1~deb11u1_arm64.deb',
      'http://snapshot.debian.org/archive/debian-security/20231027T235634Z/pool/updates/main/c/chromium/chromium-common_118.0.5993.117-1~deb11u1_arm64.deb',
    ])
  })

  test('Chromium 119.0 arm64 .deb urls', async () => {
    expect(await getDebUrlsForVersionAndArch(119, 0, DEBIAN_VERSION, 'arm64')).toEqual([
      'http://snapshot.debian.org/archive/debian-security/20231130T164103Z/pool/updates/main/c/chromium/chromium_119.0.6045.199-1~deb11u1_arm64.deb',
      'http://snapshot.debian.org/archive/debian-security/20231130T164103Z/pool/updates/main/c/chromium/chromium-common_119.0.6045.199-1~deb11u1_arm64.deb',
    ])
  })

})
