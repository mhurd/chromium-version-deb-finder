.DEFAULT: freshinstall
.PHONY: freshinstall
freshinstall:
	-make stop
	-make remove
	make prepare

.PHONY: prepare
prepare:
	docker build -t chromium-version-deb-finder .

.PHONY: remove
remove:
	docker rm chromium-version-deb-finder

.PHONY: stop
stop:
	docker stop chromium-version-deb-finder

.PHONY: test
test:
	make
	docker run -it --rm chromium-version-deb-finder /bin/sh -c "npm test"

.PHONY: sample
sample:
	make
	docker run -it --rm chromium-version-deb-finder /bin/sh -c "node -e '(async () => { console.log(await require(\"./index.js\").getDebUrlsForVersionAndArch(101, 0, 11, \"arm64\")); })()'"
