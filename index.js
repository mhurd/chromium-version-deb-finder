const axios = require('axios')
const cheerio = require('cheerio')

const compareVersions = (a, b) => a.localeCompare(b, undefined, { numeric: true, sensitivity: 'base' })

const SNAPSHOT_BASE_URL = 'http://snapshot.debian.org'

const getMostRecentDebUrlsForVersionAndArch = async (chromiumMajorVersion, chromiumMinorVersion, debianVersion, content, architecture) => {
  const binaryPageUrls = await getBinaryPageUrls(chromiumMajorVersion, chromiumMinorVersion, debianVersion, content)
  for (const url of binaryPageUrls) {
    const debUrls = await getFilteredDebFileUrls(url, architecture)
    if (debUrls.length > 0) {
      return debUrls
    }
  }
}

const getBinaryPageUrls = async (chromiumMajorVersion, chromiumMinorVersion, debianVersion, baseUrlContent) => {
  const chromiumVersion = `${chromiumMajorVersion}.${chromiumMinorVersion}.[0-9]+`
  const $ = cheerio.load(baseUrlContent.data)
  const distSpecificUrls = $('a[href]')
    .toArray()
    .map(element => $(element).attr('href'))
    .filter(href => href.match(`${chromiumVersion}.[0-9]+`) && href.match(`[0-9]+~deb${debianVersion}+u[0-9]+`))
    .sort(compareVersions)
    .reverse()
  return distSpecificUrls.map(url => `${SNAPSHOT_BASE_URL}${url.slice(5)}`)
}

const getFilteredDebFileUrls = async (binaryPageUrl, architecture = ".*") => {
  const versionString = binaryPageUrl.split("/").reverse()[1]
  const deb_restriction = "chromium|chromium-common"
  const content = await axios.get(binaryPageUrl)
  const $ = cheerio.load(content.data)
  return $('a[href]')
    .toArray()
    .map(element => $(element).attr('href'))
    .filter(href => href.match(`.*(${deb_restriction})_${versionString}_${architecture}\\.deb`))
    .map(href => `${binaryPageUrl.match(/(https?:\/\/[^/]+)/)[0]}${href}`)
    .filter(fullUrl => fullUrl.includes("debian-security"))
}

let cache = {
  content: null,
  timestamp: null,
}

// Cache so tests hit the initial index page only once and don't get throttled
const getSnapshotIndexContentWithCache = async () => {
  const now = Date.now()
  const cacheDuration = 3 * 60 * 1000 // Cache duration in milliseconds (3 minutes)
  if (cache.content === null || now - cache.timestamp > cacheDuration) {
    cache.content = await axios.get(`${SNAPSHOT_BASE_URL}/binary/chromium/`)
    cache.timestamp = now
  } 
  return cache.content
}

const getDebUrlsForVersionAndArch = async (chromiumMajorVersion, chromiumMinorVersion, debianVersion, architecture) => {
  const content = await getSnapshotIndexContentWithCache()
  return await getMostRecentDebUrlsForVersionAndArch(chromiumMajorVersion, chromiumMinorVersion, debianVersion, content, architecture)
}

module.exports = {
  getDebUrlsForVersionAndArch
}
